var consolere = require('console-remote-client').connect('console.re', '80', 'tcy-demo-data');

var noble = require('noble');
var ledManager = require('./ledManager.js');
var serverConnection = require('./serverConnection.js');

var toothbrushList = {};
var dataObject = {}



exports.main = function(peripheral) {
    peripheral._events = []

    var toothbrushID = peripheral.id;

    console.re.log("Found toothbrush with id: " + toothbrushID);

    peripheral.once('connect', function() {
        console.re.log('on -> connect');

        noble.startScanning([], true);
        this.updateRssi();
    });

    peripheral.once('disconnect', function() {
        console.re.log('on -> disconnect');

        toothbrushList[peripheral.id] = false;

        //sessionEnd function
        if (dataObject[peripheral.id] != undefined) {
            console.re.log("this toothbrush has logged " + dataObject[peripheral.id].sessions.length + " session")

            var dataToPost = dataObject[peripheral.id];

            //serverConnection.queueData('TOOTHBRUSH', peripheral.id, 'SESSION', dataToPost)

            dataObject[peripheral.id] = null

        }

    });

    peripheral.once('rssiUpdate', function(rssi) {
        console.re.log('on -> RSSI update ' + rssi);
        this.discoverServices();
    });

    var sampleCountLed = 0;

    peripheral.once('servicesDiscover', function(services) {
        //console.re.log('on -> peripheral services discovered ' + services);

        for (var i = 0; i < services.length; i++) {
            // console.re.log(i + " --  " + services[i].uuid)

            services[i].on('includedServicesDiscover', function(includedServiceUuids) {
                //console.re.log('on -> service included services discovered ' + includedServiceUuids);
                this.discoverCharacteristics();
            });

            services[i].discoverIncludedServices();

            services[i].on('characteristicsDiscover', function(characteristics) {
                //console.re.log('on -> service characteristics discovered ' + characteristics);

                for (var k = 0; k < characteristics.length; k++) {
                    //console.re.log(characteristics[k].uuid + " -- " + characteristics[k].name)

                    if (characteristics[k].uuid.indexOf('54080') > -1 || characteristics[k].uuid.indexOf('540b0') > -1 || characteristics[k].uuid.indexOf('2a19') > -1 || characteristics[k].uuid.indexOf('54010') > -1) {
                        characteristics[k].read();
                    }

                    characteristics[k].notify(true);

                    if (characteristics[k].uuid == '477ea600a26011e4ae370002a5d54130') {
                        console.re.log("NOTIFY");
                        characteristics[k].notify(true);
                    }

                    characteristics[k].on('read', function(data, isNotification) {

                        var stringData = JSON.stringify(data);
                        var jsonObject = JSON.parse(stringData).data;

                        // /* CONSTRUCT THE DATA OBJECT */
                        // if (!dataObject.hasOwnProperty(peripheral.id)) {
                        //     dataObject[peripheral.id] = {
                        //         sessionIndex: -1,
                        //         lastSample: 0,
                        //         sessions: [],
                        //     };

                        // }

                        sampleCountLed++;

                        if (sampleCountLed % 20 == 0) {
                            if (peripheral.id == '24e5aac1e455') {
                                ledManager.controlLed(0, 255, 0);
                                setTimeout(ledManager.turnOFF, 100);
                            } else {
                                ledManager.controlLed(255, 0, 0);
                                setTimeout(ledManager.turnOFF, 100);
                            }
                        }

                        /* READ ALL RELEVANT CHARACTERISTIC */

                        if (this.uuid.indexOf('54010') > -1) { //ON - OFF BUTTON
                            var storeJson = {
                                t: Date.now(),
                                state: jsonObject[0]
                            }

                            saveData(peripheral, 'powerBtn', storeJson)

                            console.re.log("ON/OFF BUTTON PRESSED:" + jsonObject[0]); // 1=OFF, 2=ON, 7=LIGHTS OFF, 3==ONCHARGER?

                        } else if (this.uuid.indexOf('540b0') > -1) { //INTENSITY SWITCH
                            var storeJson = {
                                t: Date.now(),
                                state: jsonObject[0]
                            }

                            saveData(peripheral, 'intensityBtn', storeJson)

                            console.re.log("LEVEL CHANGED: " + jsonObject[0]);

                        } else if (this.uuid.indexOf('54080') > -1) { //MODE BUTTON 
                            var storeJson = {
                                t: Date.now(),
                                state: jsonObject[0]
                            }

                            saveData(peripheral, 'modeBtn', storeJson)

                            console.re.log("MODE BUTTON CLICKED: " + jsonObject[0])

                        } else if (this.uuid.indexOf('2a19') > -1) { //BATTERY LEVEL
                            var storeJson = {
                                t: Date.now(),
                                state: jsonObject[0]
                            }

                            saveData(peripheral, 'batLevel', storeJson)

                            console.re.log("BATTERY LEVEL: " + jsonObject[0])

                        } else if (this.uuid.indexOf('54130') > -1) {
                            if (data.length == 16) {
                                //console.re.log(jsonObject);

                                var storeJson = {
                                    t: Date.now(),
                                    acc_x: jsonObject[3],
                                    acc_y: jsonObject[4],
                                    acc_z: jsonObject[5],
                                    gyro_x: jsonObject[6],
                                    gyro_y: jsonObject[7],
                                    gyro_z: jsonObject[8],
                                }

                                saveData(peripheral, 'sensorData', storeJson)

                                //console.re.log(peripheral.id + " -- " + dataObject[peripheral.id].sessionIndex + ' -- ' + dataObject[peripheral.id].sessions[dataObject[peripheral.id].sessionIndex].sensorData.length)

                            } else {

                                //HAVE TO INTEGRATE THE PRESSURE HERE!

                                console.re.log(jsonObject);
                            }
                        }

                        // /* CREATE A NEW SESSION WHEN LAST ONE EXPIRED */
                        // if (Date.now() - dataObject[peripheral.id].lastSample > 10000) {
                        //     console.re.log("ADD")
                        //     dataObject[peripheral.id].sessionIndex = dataObject[peripheral.id].sessionIndex + 1;
                        // }

                        // dataObject[peripheral.id].lastSample = Date.now();

                    });
                }
            });
        }
    });

    if (toothbrushList[peripheral.uuid] != true) {
        toothbrushList[peripheral.uuid] = true;
        peripheral.connect();
    }
}


function saveData(peripheral, attr, storeJson) {

    if (dataObject[peripheral.id] == null) {
        dataObject[peripheral.id] = {
            uuid: peripheral.id,
            startTime: Date.now(),
            sensorData: [],
            pressureData: [],
            powerBtn: [],
            intensityBtn: [],
            modeBtn: [],
            batLevel: [],
        }
    }

    dataObject[peripheral.id][attr].push(storeJson);

}


// function storeToothbrushData(peripheral, attr, storeJson) {

//     //if there is no sessions with index sessionindex make it
//     var found = false;

//     for (var i = 0; i < dataObject[peripheral.id].sessions.length; i++) {

//         if (dataObject[peripheral.id].sessions[i] && dataObject[peripheral.id].sessions[i].index == dataObject[peripheral.id].sessionIndex) {
//             found = true
//         }
//     }

//     if (!found) {
//         var newSession = {
//             index: dataObject[peripheral.id].sessionIndex,
//             state: 'start',
//             uuid: peripheral.id,
//             startTime: Date.now(),
//             sensorData: [],
//             pressureData: [],
//             powerBtn: [],
//             intensityBtn: [],
//             modeBtn: [],
//             batLevel: [],
//         }

//         dataObject[peripheral.id].sessions[dataObject[peripheral.id].sessionIndex] = newSession;

//         console.re.log("make a new empty session")
//     }

//     var session = dataObject[peripheral.id].sessions[dataObject[peripheral.id].sessionIndex];

//     if (session.length > 0) {
//         console.re.log(session[session.length - 1])
//         if (session[attr][session[attr].length - 1].t != storeJson.t) {
//             session[attr].push(storeJson)
//         } else {
//             console.re.log("do nothing - duplicate")
//         }
//     } else {
//         session[attr].push(storeJson)
//     }
// }