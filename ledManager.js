var ws281x = require('rpi-ws281x-native');
var NUM_LEDS = 12;
var pixelData = new Uint32Array(NUM_LEDS);
var iteration = 0;
var brightness = 128;

ws281x.init(NUM_LEDS);

exports.controlLed = function(red, green, blue) {
    for (var i = 0; i < NUM_LEDS; i++) {
        pixelData[i] = color(red, green, blue);
    }

    ws281x.render(pixelData);
}

exports.loader = function(red, green, blue) {
    iteration = 0;

    module.exports.countUpLed(red, green, blue);
}



exports.countUpLed = function(red, green, blue) {
    iteration++;

    for (var i = 0; i < iteration; i++) {
        pixelData[i] = color(red, green, blue);
    }

    ws281x.render(pixelData);

    if (iteration < 12) {
        setTimeout(function() {
            module.exports.countUpLed(red, green, blue);
        }, 300)
    }

}


exports.turnOFF = function() {
    for (var i = 0; i < NUM_LEDS; i++) {
        pixelData[i] = color(0, 0, 0);
    }

    ws281x.render(pixelData);
}

// generate integer from RGB value
function color(r, g, b) {
    r = r * brightness / 255;
    g = g * brightness / 255;
    b = b * brightness / 255;
    return ((r & 0xff) << 16) + ((g & 0xff) << 8) + (b & 0xff);
}