var consolere = require('console-remote-client').connect('console.re', '80', 'tcy-demo-data');


var noble = require('noble');
var ledManager = require('./ledManager.js');

var skinSensorList = {};


exports.main = function(peripheral) {
    var skinSensorId = peripheral.id;

    console.re.log("Found skin sensor with id: " + skinSensorId);

    peripheral.disconnect(function(error) {
        console.re.log('on -> disconnect skin sensor');

        skinSensorList[peripheral.id] = false;
    });

    if (skinSensorList[peripheral.id] != true) {
        peripheral.connect(function(error) {
            noble.startScanning([], true);

            skinSensorList[peripheral.uuid] = true;

            console.re.log('connected to peripheral: ' + peripheral.uuid);

            peripheral.discoverServices(['1600'], function(error, services) {
                var deviceInformationService = services[0];
                console.re.log('discovered device information service');

                deviceInformationService.discoverCharacteristics(['1601'], function(error, characteristics) {
                    var dataCharacteristic = characteristics[0];

                    readSkinSensor(dataCharacteristic, peripheral.uuid);
                });
            });
        });
    }
}


var previousDataObject = {};
var timer = 0;

function readSkinSensor(dataCharacteristic, id) {
    dataCharacteristic.read(function(error, data) {
        // data is a buffer
        var dataString = JSON.stringify(data);

        console.re.log(JSON.stringify(data))
        console.re.log('manufacture name is: ' + data.toString('utf8'));

        var jsonData = JSON.parse(dataString);

        console.re.log("J")
        console.re.log(jsonData);
        if (previousDataObject[id] != dataString) {
            if (jsonData.data[6] == 0 && jsonData.data[0] != 0) {
                //calibration measurement
                ledManager.loader(255, 100, 255);
            } else {
                ledManager.turnOFF();
            }
        }

        previousDataObject[id] = dataString;
        //check if it changed to see if new value has been read

        //push to socket

        //store in data object

    });

    console.re.log(skinSensorList)

    if (skinSensorList[id] == true) {
        setTimeout(function() {
            readSkinSensor(dataCharacteristic, id);
        }, 500)
    }
}



function saveData(peripheral, attr, storeJson) {

    if (dataObject[peripheral.id] == null) {
        dataObject[peripheral.id] = {
            uuid: peripheral.id,
            startTime: Date.now(),
            sensorData: [],

        }
    }

    dataObject[peripheral.id][attr].push(storeJson);

}