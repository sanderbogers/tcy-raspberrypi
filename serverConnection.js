var request = require("request");

var dataList = [];

exports.queueData = function(provider, identifier, type, data) {

    var dataObject = {
        provider: provider,
        identifier: identifier,
        type: type,
        data: data,
    }

    dataList.push(dataList);

    postData();

    //store locally?
}


setInterval(postData, 10000);

var inProgress = false;

function postData(){
    console.re.log("see if any data needs to be posted");

    if (!inProgress) {
        if (dataList.length > 0) {
            var options = {
                method: 'POST',
                url: 'http://192.168.0.101:4001/storedata', //for now, my laptop IP
                headers: {
                    webtoken: 'ad3a485def3828112f1a6f63b3829f446329036e',
                    projectid: '5afa105e19a73736e114dc6c'
                },
                json: true,
                body: {
                    provider: dataList[0].provider,
                    identifier: dataList[0].identifier,
                    type: dataList[0].type,
                    data: dataList[0].data
                }
            };

            inProgress = true;

            request(options, function(error, response, body) {
                if (error) throw new Error(error);

                inProgress = false;

                if (response.statusCode === 200) {
                    dataList = dataList.shift();

                    console.log('after shift: ');
                    console.log(dataList);

                    if (dataList.length > 0) {
                        postData();
                    }

                    console.log(body);

                } else {}

            });
        }
        else{
            console.re.log('no data in post queue');
        }
    }
}