var consolere = require('console-remote-client').connect('console.re','80','tcy-demo-data');

// var myIP = require('my-ip');

// console.re.log(myIP());// return external IPv4
// console.re.log(myIP('IPv6'));// return external IPv6 
// console.re.log(myIP(null,true));// return internal IPv4
// console.re.log(myIP('IPv6',true));// return internal IPv6


var noble = require('noble');
var request = require("request");


var skinSensor = require('./skinSensor.js');
var toothBrush = require('./toothbrush.js');
var shaver = require('./shaver.js');

var ledManager = require('./ledManager.js');

ledManager.controlLed(0, 0, 0);


noble.on('stateChange', function(state) {
    console.re.log('on -> stateChange: ' + state);

    if (state === 'poweredOn') {
        noble.startScanning([], true);
    } else {
        noble.stopScanning();
    }
});

noble.on('scanStart', function() {
    console.re.log('on -> scanStart');
});

noble.on('scanStop', function() {
    console.re.log('on -> scanStop');
});

noble.on('discover', function(peripheral) {

    var deviceName = peripheral.advertisement.localName;

    if (deviceName != undefined) {
        console.re.log('device found: ' + deviceName);
    }

    // if (deviceName == 'MyService') {
    //     noble.stopScanning();
    //     skinSensor.main(peripheral);
    // }

    if (deviceName == "Philips Sonicare") {
        noble.stopScanning();
        toothBrush.main(peripheral);
    }

    if (deviceName == "Philips S7921") {
        noble.stopScanning();
        shaver.main(peripheral);
    }
});