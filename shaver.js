var consolere = require('console-remote-client').connect('console.re', '80', 'tcy-demo-data');

var noble = require('noble');
var ledManager = require('./ledManager.js');

var connected = false;
var dataObject = {}

var shaverList = {};

exports.main = function(peripheral) {
    peripheral._events = []

    console.re.log("Found shaver with id: " + peripheral.id);


    peripheral.once('connect', function() {
        console.re.log('on -> connect');

        ledManager.controlLed(0, 0, 255);

        setTimeout(ledManager.turnOFF, 100);

        noble.startScanning([], true);
        this.updateRssi();
    });


    peripheral.once('disconnect', function() {
        shaverList[peripheral.id] = false;

        console.re.log('on -> disconnect');
    });


    peripheral.once('rssiUpdate', function(rssi) {
        console.re.log('on -> RSSI update ' + rssi);

        //if this errors on no rssi, reset the bluetooth adapter and try again : sudo invoke-rc.d bluetooth restart
        this.discoverServices();
    });


    var sampleLedCount = 0;

    peripheral.once('servicesDiscover', function(services) {
        console.re.log('on -> peripheral services discovered ' + services);

        for (var i = 0; i < services.length; i++) {
            console.re.log(i + " --  " + services[i].uuid)

            services[i].on('includedServicesDiscover', function(includedServiceUuids) {
                console.re.log('on -> service included services discovered ' + includedServiceUuids);
                this.discoverCharacteristics();
            });

            services[i].discoverIncludedServices();

            services[i].on('characteristicsDiscover', function(characteristics) {
                console.re.log('on -> service characteristics discovered ' + characteristics);

                for (var k = 0; k < characteristics.length; k++) {
                    console.re.log(characteristics[k].uuid + " -- " + characteristics[k].name)

                    characteristics[k].notify(true);

                    characteristics[k].on('read', function(data, isNotification) {

                        var stringData = JSON.stringify(data);
                        var jsonObject = JSON.parse(stringData).data;

                        // console.re.log(" ********* ")
                        // console.re.log(jsonObject);

                        if (this.uuid.indexOf('8d560102') > -1) { //ON - OFF BUTTON
                            // var storeJson = {
                            //     t: Date.now(),
                            //     current: jsonObject[0]
                            // }
                            ledManager.controlLed(0, 0, 255);

                            setTimeout(ledManager.turnOFF, 100);

                            console.re.log("Motor Current");
                            console.re.log(jsonObject);


                        } else if (this.uuid.indexOf('8d560104') > -1) { //INTENSITY SWITCH
                            console.re.log("Motor RPM"); //this is also a write characteristic
                            console.re.log(jsonObject);
                        }
                    });
                }
            });
        }
    });

    if (shaverList[peripheral.uuid] != true) {
        shaverList[peripheral.uuid] = true;
        peripheral.connect();
    }

}


function saveData(peripheral, attr, storeJson) {            //adjust to shaver specific data

    if (dataObject[peripheral.id] == null) {
        dataObject[peripheral.id] = {
            uuid: peripheral.id,
            startTime: Date.now(),
            sensorData: [],
            pressureData: [],
            powerBtn: [],
            intensityBtn: [],
            modeBtn: [],
            batLevel: [],
        }
    }

    dataObject[peripheral.id][attr].push(storeJson);

}